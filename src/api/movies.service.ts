import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {ApiResult} from '../models/ApiResult';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})

export class MoviesService {

    apiUrl = 'https://api.themoviedb.org/3/search/movie';
    apiUrlDetails = 'https://api.themoviedb.org/3/movie/';
    apiKey = '?api_key=b98d8f222b1204d731f1b6ae4bde9092';
    apiLang = '&language=en-US';
    apiQuery = '&query=';
    apiPage = '&page=1';
    apiAdult = '&include_adult=false';

    constructor(private http: HttpClient) {
    }

    getTopRated():Observable<ApiResult>{
        return this.http.get<ApiResult>(`${this.apiUrlDetails}top_rated${this.apiKey}${this.apiLang}${this.apiPage}`)
        .pipe(
            retry(1),
            catchError(this.handleError),
        );
    }

    getVideo(id): Observable<ApiResult> {
        return this.http.get<ApiResult>(`${this.apiUrlDetails}${id}/videos${this.apiKey}${this.apiLang}`)
            .pipe(
                retry(1),
                catchError(this.handleError),
            );
    }

    getSimilarMovies(id): Observable<ApiResult> {
        return this.http.get<ApiResult>(`${this.apiUrlDetails}${id}/similar${this.apiKey}${this.apiLang}${this.apiPage}`)
            .pipe(
                retry(1),
                catchError(this.handleError),
            );
    }

    searchMovies(searchName: string): Observable<ApiResult> {
        return this.http.get<ApiResult>(
            `${this.apiUrl}${this.apiKey}${this.apiLang}+'&query='+${searchName}${this.apiPage}${this.apiAdult}`)
            .pipe(
                retry(1),
                catchError(this.handleError),
            );
    }

    getMovieDetails(id): Observable<ApiResult> {
        return this.http.get<ApiResult>(`${this.apiUrlDetails}${id}${this.apiKey}${this.apiLang}`).pipe(
            retry(1),
            catchError(this.handleError),
        );
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }
}
