import {Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Movie } from 'src/models/Movie';
import {MoviesService} from '../../../api/movies.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {

  selectedMovie: string;
  searchName: string;
  noImage: string = '../../../assets/images/noimage.png';
  movies: Movie[];
  topTenMovies: Movie[];

  constructor(
    public restApi: MoviesService,
    public router: Router,
  ) {
  }

  recieveSearchName($event) {
    this.searchName = $event.value;
    this.searchMovies();
  }

  searchMovies(): void {
    this.restApi.searchMovies(this.searchName).subscribe((data: any) => {
      this.movies = data.results.map((movieData) => {
        const {id} = movieData;
        return ({
          ...movieData,
          id,
        });
      });
    });
  }

  getLatestMovies(): void {
    this.restApi.getTopRated().subscribe((data: any) => {
      this.topTenMovies = data.results.slice(0, 10).map((movieData) => {
        const {id} = movieData;
        return ({
          ...movieData,
          id,
        });
      });
    });
  }

  onSelect(id: string): void {
    this.selectedMovie = id;
    this.router.navigate(['movie-details', id]);
  }

  ngOnInit() {
    this.getLatestMovies();
  }
}
