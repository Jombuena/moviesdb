import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-searchbar',
    templateUrl: './searchbar.component.html',
    styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {

    searchName: string;

    @Output() searchOutput = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit() {
    }

    handleSearch(searchName) {
        if(this.searchName !== null){
            this.searchName = searchName;
            this.sendName();
        }
    }

    sendName() {
        this.searchOutput.emit(this.searchName);
    }

}
