import {Component, OnInit} from '@angular/core';
import {MoviesService} from 'src/api/movies.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-movie-details',
    templateUrl: './movie-details.page.html',
    styleUrls: ['./movie-details.page.scss'],
})

export class MovieDetailsPage implements OnInit {

    selectedMovie: string;
    movie: {
        id: string;
        title: string;
        spoken_languages: [
            {
                id: string;
                name: string
            }
        ],
        poster_path: string;
        overview: string;
        release_date: string;
        vote_average: number;
        genres: [
            {
                id: string;
                name: string
            }
        ],
    };
    related: {
        results: [
            {
                id: string;
                title: string;
                poster_path: string;
            }
        ]
    };
    video: {
        results: [
            {
                key: string;
                size: number;
                type: string;
            }
        ]
    };

    videoUrl: any;

    constructor(
        public movieService: MoviesService,
        public route: ActivatedRoute,
        public router: Router,
        private dom: DomSanitizer) {
    }

    loadDetails(id: any): void {
        this.movieService.getMovieDetails(id).subscribe((data: any) =>
            this.movie = data,
        );
    }

    loadSimilar(id: any): void {
        this.movieService.getSimilarMovies(id).subscribe((data: any) =>
            this.related = data,
        );
    }

    loadVideo(id: any): void {
        this.movieService.getVideo(id).subscribe((data: any) => {
            this.video = data;
            const videoKey = this.video.results[0].key;
            const videoUrl = `https://www.youtube.com/embed/${videoKey}`;
            this.videoUrl = this.dom.bypassSecurityTrustResourceUrl(videoUrl);
        },
    );
    }

    onSelect(id: string): void {
        this.selectedMovie = id;
        this.router.navigate(['movie-details', id]);
    }

    goBack() {
        this.router.navigate(['tabs/tab1']);
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.loadVideo(params.id);
            this.loadDetails(params.id);
            this.loadSimilar(params.id);
        });
    }
}
