import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { MovieListComponent} from '../components/movie-list/movie-list.component';
import {SearchbarComponent} from '../components/searchbar/searchbar.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: Tab1Page}])
    ],
    exports: [
        MovieListComponent
    ],
    declarations: [Tab1Page, MovieListComponent, SearchbarComponent]
})
export class Tab1PageModule {}
