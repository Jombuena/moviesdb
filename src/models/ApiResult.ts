export class ApiResult {
    results: [Object];
    url: string;
    id: string;
}
