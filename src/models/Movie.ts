export class Movie{
    id: string;
    poster_path: string;
    title: string;
    date: string;
}