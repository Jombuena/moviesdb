(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_movie-details_movie-details_module_ts"],{

/***/ 4333:
/*!*************************************************************!*\
  !*** ./src/app/pages/movie-details/movie-details.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MovieDetailsPageModule": () => (/* binding */ MovieDetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _movie_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./movie-details.page */ 272);







const routes = [{
  path: '',
  component: _movie_details_page__WEBPACK_IMPORTED_MODULE_0__.MovieDetailsPage
}];
let MovieDetailsPageModule = class MovieDetailsPageModule {};
MovieDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)],
  declarations: [_movie_details_page__WEBPACK_IMPORTED_MODULE_0__.MovieDetailsPage]
})], MovieDetailsPageModule);


/***/ }),

/***/ 272:
/*!***********************************************************!*\
  !*** ./src/app/pages/movie-details/movie-details.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MovieDetailsPage": () => (/* binding */ MovieDetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _movie_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./movie-details.page.html?ngResource */ 9934);
/* harmony import */ var _movie_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movie-details.page.scss?ngResource */ 9289);
/* harmony import */ var _movie_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_movie_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var src_api_movies_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/api/movies.service */ 1352);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ 4497);
var _class;







let MovieDetailsPage = (_class = class MovieDetailsPage {
  constructor(movieService, route, router, dom) {
    this.movieService = movieService;
    this.route = route;
    this.router = router;
    this.dom = dom;
  }
  loadDetails(id) {
    this.movieService.getMovieDetails(id).subscribe(data => this.movie = data);
  }
  loadSimilar(id) {
    this.movieService.getSimilarMovies(id).subscribe(data => this.related = data);
  }
  loadVideo(id) {
    this.movieService.getVideo(id).subscribe(data => {
      this.video = data;
      const videoKey = this.video.results[0].key;
      const videoUrl = `https://www.youtube.com/embed/${videoKey}`;
      this.videoUrl = this.dom.bypassSecurityTrustResourceUrl(videoUrl);
    });
  }
  onSelect(id) {
    this.selectedMovie = id;
    this.router.navigate(['movie-details', id]);
  }
  goBack() {
    this.router.navigate(['tabs/tab1']);
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.loadVideo(params.id);
      this.loadDetails(params.id);
      this.loadSimilar(params.id);
    });
  }
}, _class.ctorParameters = () => [{
  type: src_api_movies_service__WEBPACK_IMPORTED_MODULE_2__.MoviesService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router
}, {
  type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__.DomSanitizer
}], _class);
MovieDetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-movie-details',
  template: _movie_details_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [(_movie_details_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default())]
})], MovieDetailsPage);


/***/ }),

/***/ 9289:
/*!************************************************************************!*\
  !*** ./src/app/pages/movie-details/movie-details.page.scss?ngResource ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Imports
var ___CSS_LOADER_API_SOURCEMAP_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/sourceMaps.js */ 9579);
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ 931);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(___CSS_LOADER_API_SOURCEMAP_IMPORT___);
// Module
___CSS_LOADER_EXPORT___.push([module.id, "/** CUSTOM VARIABLES **/\n/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #3880ff;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9;\n}\n\nion-toolbar div {\n  display: flex;\n}\nion-toolbar div .movie-button {\n  padding-right: 1rem;\n}\n\n.movie-details {\n  width: 100%;\n  margin: auto;\n}\n\n.movie-data-container {\n  display: block;\n  width: 90%;\n  margin: auto;\n  padding: 2rem;\n}\n\n.data-row {\n  display: block;\n  width: 80%;\n  margin: auto;\n}\n\n.info {\n  background-color: rgba(255, 0, 0, 0.5);\n  border-radius: 5px;\n  padding: 2rem;\n}\n\n.image-container {\n  margin: auto;\n}\n\n.movie-release {\n  margin: 0 0 10px 4%;\n}\n\n.movie-average {\n  margin: 0 0 10px 4%;\n}\n\n.genres-container {\n  margin-left: 4%;\n}\n.genres-container li .genre-badge {\n  margin: 0 5px 0 0;\n  padding: 5px;\n}\n\n.movie-image {\n  width: -webkit-fill-available;\n  width: -moz-available;\n}\n\n.movie-title {\n  text-align: center;\n  font-size: 24px;\n}\n\n.movie-video {\n  align-items: center;\n  justify-content: center;\n  width: 250px;\n  height: 175px;\n}\n\n.movie-related-image {\n  max-width: 120px;\n  min-height: 180px;\n  margin: 15px;\n}\n\nul {\n  display: inline-flex;\n  padding-inline-start: 5px;\n  margin: 0;\n}\n\n.langs-container {\n  margin: 0 0 10px 4%;\n}\n.langs-container li {\n  list-style: none;\n  display: inline;\n}\n.langs-container li:after {\n  content: \",\";\n}\n.langs-container li:last-child:after {\n  content: \".\";\n}\n.langs-container li span {\n  display: inline-list-item;\n}\n\n.overview-container {\n  margin: 10px 0 10px 4%;\n}\n\n.trailer-container {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  margin: 2rem 0;\n  height: 60%;\n}\n.trailer-container iframe {\n  width: 45.25rem;\n  height: 25.5rem;\n  margin: 15px auto;\n}\n\n.movie-overview {\n  font-size: 18px;\n  text-align: justify;\n  text-justify: inter-word;\n}\n\n.related-list-title {\n  margin: 0 0 10px 4%;\n}\n\n.related-container {\n  margin-top: 5%;\n}\n.related-container .related-list {\n  display: flex;\n  list-style: none;\n  flex-wrap: wrap;\n  align-items: center;\n  justify-content: flex-start;\n}\n.related-container .related-list li:nth-of-type(1n+6) {\n  display: none;\n}\n.related-container .related-list .related-card {\n  width: content-box;\n  display: flex;\n  flex-direction: column;\n  margin: 15px;\n  cursor: pointer;\n}\n\n@media screen and (min-width: 480px) {\n  .movie-image {\n    width: 100%;\n    height: 100%;\n  }\n  .image-container {\n    width: 90%;\n    margin: auto;\n  }\n  .overview-container {\n    max-width: 94%;\n    margin: 10px 0 10px 4%;\n  }\n  .related-container {\n    margin-top: 5%;\n  }\n  .related-container .related-list {\n    display: flex;\n    list-style: none;\n    flex-wrap: wrap;\n    align-items: center;\n    justify-content: flex-start;\n  }\n  .related-container .related-list li:nth-of-type(1n+9) {\n    display: none;\n  }\n  .related-container .related-list .related-card {\n    width: content-box;\n    display: flex;\n    flex-direction: column;\n    margin: 10px;\n    cursor: pointer;\n  }\n}\n@media screen and (min-width: 991px) {\n  .related-list-title {\n    max-width: 95%;\n    margin: 0 0 0 0;\n  }\n  .overview-container {\n    max-width: 85%;\n    margin: 10px 0 10px 4%;\n  }\n  .list-container {\n    max-width: 73%;\n  }\n  .movie-release {\n    max-width: 73%;\n  }\n  .movie-average {\n    max-width: 73%;\n  }\n  .related-container {\n    margin-top: 2%;\n  }\n  .related-container strong {\n    margin-top: 10px;\n  }\n}\n@media screen and (min-width: 1200px) {\n  .overview-container {\n    max-width: 80%;\n    margin: 10px 0 10px 4%;\n  }\n}", "",{"version":3,"sources":["webpack://./src/theme/variables.scss","webpack://./src/app/pages/movie-details/movie-details.page.scss"],"names":[],"mappings":"AAGA,uBAAA;AAGA,0BAAA;AACA;EACE,cAAA;EACA,4BAAA;EACA,qCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,gBAAA;EACA,8BAAA;EACA,uCAAA;EACA,uCAAA;EACA,iDAAA;EACA,oCAAA;EACA,mCAAA;EAEA,eAAA;EACA,6BAAA;EACA,sCAAA;EACA,sCAAA;EACA,gDAAA;EACA,mCAAA;EACA,kCAAA;EAEA,cAAA;EACA,4BAAA;EACA,oCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,cAAA;EACA,4BAAA;EACA,oCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,aAAA;EACA,2BAAA;EACA,mCAAA;EACA,oCAAA;EACA,8CAAA;EACA,iCAAA;EACA,gCAAA;EAEA,WAAA;EACA,yBAAA;EACA,gCAAA;EACA,kCAAA;EACA,4CAAA;EACA,+BAAA;EACA,8BAAA;EAEA,aAAA;EACA,2BAAA;EACA,qCAAA;EACA,oCAAA;EACA,8CAAA;EACA,iCAAA;EACA,gCAAA;EAEA,YAAA;EACA,0BAAA;EACA,oCAAA;EACA,mCAAA;EACA,uCAAA;EACA,gCAAA;EACA,+BAAA;ACZF;;AAhEA;EACE,aAAA;AAmEF;AAlEE;EACE,mBAAA;AAoEJ;;AAhEA;EACE,WAAA;EACA,YAAA;AAmEF;;AAhEA;EACE,cAAA;EACA,UAAA;EACA,YAAA;EACA,aAAA;AAmEF;;AAhEA;EACE,cAAA;EACA,UAAA;EACA,YAAA;AAmEF;;AAhEA;EACE,sCAAA;EACA,kBAAA;EACA,aAAA;AAmEF;;AAhEA;EACE,YAAA;AAmEF;;AAhEA;EACE,mBDlCU;ACqGZ;;AAhEA;EACE,mBDtCU;ACyGZ;;AAhEA;EACE,eAAA;AAmEF;AAhEI;EACE,iBAAA;EACA,YAAA;AAkEN;;AA7DA;EACE,6BAAA;EACA,qBAAA;AAgEF;;AA7DA;EACE,kBAAA;EACA,eAAA;AAgEF;;AA7DA;EACE,mBAAA;EACA,uBAAA;EACA,YAAA;EACA,aAAA;AAgEF;;AA7DA;EACE,gBAAA;EACA,iBAAA;EACA,YAAA;AAgEF;;AA7DA;EACE,oBAAA;EACA,yBAAA;EACA,SAAA;AAgEF;;AA7DA;EACE,mBDlFU;ACkJZ;AA9DE;EACE,gBAAA;EACA,eAAA;AAgEJ;AA9DI;EACE,YAAA;AAgEN;AA7DI;EACE,YAAA;AA+DN;AA5DI;EACE,yBAAA;AA8DN;;AAzDA;EACE,sBAAA;AA4DF;;AAzDA;EAGE,aAAA;EACA,sBAAA;EACA,uBAAA;EACA,cAAA;EACA,WAAA;AA0DF;AAxDE;EACE,eAAA;EACA,eAAA;EACA,iBAAA;AA0DJ;;AAtDA;EACE,eAAA;EACA,mBAAA;EACA,wBAAA;AAyDF;;AAtDA;EACE,mBDjIU;AC0LZ;;AArDA;EACE,cAAA;AAwDF;AAtDE;EACE,aAAA;EACA,gBAAA;EACA,eAAA;EACA,mBAAA;EACA,2BAAA;AAwDJ;AAtDI;EACE,aAAA;AAwDN;AArDI;EACE,kBAAA;EACA,aAAA;EACA,sBAAA;EACA,YAAA;EACA,eAAA;AAuDN;;AAlDA;EAEE;IACE,WAAA;IACA,YAAA;EAoDF;EAjDA;IACE,UAAA;IACA,YAAA;EAmDF;EAhDA;IACE,cAAA;IACA,sBAAA;EAkDF;EA/CA;IACE,cAAA;EAiDF;EA/CE;IACE,aAAA;IACA,gBAAA;IACA,eAAA;IACA,mBAAA;IACA,2BAAA;EAiDJ;EA/CI;IACE,aAAA;EAiDN;EA9CI;IACE,kBAAA;IACA,aAAA;IACA,sBAAA;IACA,YAAA;IACA,eAAA;EAgDN;AACF;AA1CA;EAEE;IACE,cAAA;IACA,eAAA;EA2CF;EAxCE;IACE,cAAA;IACA,sBAAA;EA0CJ;EAxCE;IACE,cAAA;EA0CJ;EAxCE;IACE,cAAA;EA0CJ;EAxCE;IACE,cAAA;EA0CJ;EAvCE;IACE,cAAA;EAyCJ;EAvCI;IACE,gBAAA;EAyCN;AACF;AArCE;EACE;IACE,cAAA;IACA,sBAAA;EAuCJ;AACF","sourcesContent":["// Ionic Variables and Theming. For more info, please see:\n// http://ionicframework.com/docs/theming/\n\n/** CUSTOM VARIABLES **/\n$margin-sm: 0 0 10px 4%;\n$padding-sm: 5%;\n/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #3880ff;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9;\n}\n","@import  '../../../theme/variables.scss';\r\n\r\nion-toolbar div{\r\n  display: flex;\r\n  & .movie-button {\r\n    padding-right: 1rem;\r\n  }\r\n}\r\n\r\n.movie-details {\r\n  width:100%; \r\n  margin:auto; \r\n}\r\n\r\n.movie-data-container {\r\n  display:block; \r\n  width:90%; \r\n  margin:auto; \r\n  padding: 2rem;\r\n}\r\n\r\n.data-row{\r\n  display:block; \r\n  width:80%; \r\n  margin:auto; \r\n}\r\n\r\n.info {\r\n  background-color: rgba($color: red, $alpha: 0.5);\r\n  border-radius: 5px;\r\n  padding: 2rem\r\n}\r\n\r\n.image-container {\r\n  margin: auto;\r\n}\r\n\r\n.movie-release {\r\n  margin: $margin-sm;\r\n}\r\n\r\n.movie-average {\r\n  margin: $margin-sm;\r\n}\r\n\r\n.genres-container {\r\n  margin-left: 4%;\r\n  \r\n  li {\r\n    .genre-badge {\r\n      margin: 0 5px 0 0;\r\n      padding: 5px;\r\n    }\r\n  }\r\n}\r\n\r\n.movie-image {\r\n  width: -webkit-fill-available;\r\n  width: -moz-available;\r\n}\r\n\r\n.movie-title {\r\n  text-align: center;\r\n  font-size: 24px;\r\n}\r\n\r\n.movie-video {\r\n  align-items: center;\r\n  justify-content: center;\r\n  width: 250px;\r\n  height: 175px;\r\n}\r\n\r\n.movie-related-image {\r\n  max-width: 120px;\r\n  min-height: 180px;\r\n  margin: 15px;\r\n}\r\n\r\nul {\r\n  display: inline-flex;\r\n  padding-inline-start: 5px;\r\n  margin: 0;\r\n}\r\n\r\n.langs-container {\r\n  margin: $margin-sm;\r\n  \r\n  li {\r\n    list-style: none;\r\n    display: inline;\r\n    \r\n    &:after {\r\n      content: \",\";\r\n    }\r\n    \r\n    &:last-child:after {\r\n      content: \".\";\r\n    }\r\n    \r\n    span {\r\n      display: inline-list-item;\r\n    }\r\n  }\r\n}\r\n\r\n.overview-container {\r\n  margin: 10px 0 10px 4%;\r\n}\r\n\r\n.trailer-container {\r\n  // background-image: url('../../../assets/images/cinema.png');\r\n  // background-size: cover;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  margin: 2rem 0;\r\n  height: 60%;\r\n  \r\n  iframe {\r\n    width: 45.25rem;\r\n    height: 25.5rem;\r\n    margin: 15px auto;\r\n  }\r\n}\r\n\r\n.movie-overview {\r\n  font-size: 18px;\r\n  text-align: justify;\r\n  text-justify: inter-word;\r\n}\r\n\r\n.related-list-title {\r\n  margin: $margin-sm;\r\n  \r\n}\r\n\r\n.related-container {\r\n  margin-top: 5%;\r\n  \r\n  .related-list {\r\n    display: flex;\r\n    list-style: none;\r\n    flex-wrap: wrap;\r\n    align-items: center;\r\n    justify-content: flex-start;\r\n    \r\n    li:nth-of-type(1n+6) {\r\n      display: none;\r\n    }\r\n    \r\n    .related-card {\r\n      width: content-box;\r\n      display: flex;\r\n      flex-direction: column;\r\n      margin: 15px;\r\n      cursor: pointer;\r\n    }\r\n  }\r\n}\r\n\r\n@media screen and (min-width: 480px) {\r\n  \r\n  .movie-image {\r\n    width: 100%;\r\n    height: 100%;\r\n  }\r\n  \r\n  .image-container {\r\n    width: 90%;\r\n    margin: auto;\r\n  }\r\n  \r\n  .overview-container {\r\n    max-width: 94%;\r\n    margin: 10px 0 10px 4%;\r\n  }\r\n  \r\n  .related-container {\r\n    margin-top: 5%;\r\n    \r\n    .related-list {\r\n      display: flex;\r\n      list-style: none;\r\n      flex-wrap: wrap;\r\n      align-items: center;\r\n      justify-content: flex-start;\r\n      \r\n      li:nth-of-type(1n+9) {\r\n        display: none;\r\n      }\r\n      \r\n      .related-card {\r\n        width: content-box;\r\n        display: flex;\r\n        flex-direction: column;\r\n        margin: 10px;\r\n        cursor: pointer;\r\n      }\r\n      \r\n    }\r\n  }\r\n}\r\n\r\n@media screen and (min-width: 991px) {\r\n  \r\n  .related-list-title {\r\n    max-width: 95%;\r\n    margin: 0 0 0 0;\r\n  }\r\n  \r\n    .overview-container {\r\n      max-width: 85%;\r\n      margin: 10px 0 10px 4%;\r\n    }\r\n    .list-container {\r\n      max-width: 73%;\r\n    }\r\n    .movie-release {\r\n      max-width: 73%;\r\n    }\r\n    .movie-average {\r\n      max-width: 73%;\r\n    }\r\n    \r\n    .related-container {\r\n      margin-top: 2%;\r\n      \r\n      & strong {\r\n        margin-top: 10px;\r\n      }\r\n    }\r\n  }\r\n  \r\n  @media screen and (min-width: 1200px) {\r\n    .overview-container {\r\n      max-width: 80%;\r\n      margin: 10px 0 10px 4%;\r\n    }\r\n  }\r\n  "],"sourceRoot":""}]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___.toString();


/***/ }),

/***/ 9934:
/*!************************************************************************!*\
  !*** ./src/app/pages/movie-details/movie-details.page.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = " <ion-header>\r\n    <ion-toolbar>\r\n        <div *ngIf=\"movie\">\r\n            <ion-title> {{movie.title}}</ion-title>\r\n            <ion-button class=\"movie-button\" (click)=\"goBack()\">Go Back</ion-button>\r\n        </div>\r\n    </ion-toolbar>\r\n    \r\n</ion-header>\r\n<!--\r\n<ion-content *ngIf='movie' class=\"movie-details\">\r\n    <div class=\"row\">\r\n    </div>\r\n    <div class=\"movie-data-container\">\r\n        <ion-grid>\r\n            <ion-col size-lg=\"6\">\r\n                <div class=\"image-container\">\r\n                    <img *ngIf=\"movie.poster_path\" class=\"movie-image\" alt=\"image poster\"\r\n                    src=\"https://image.tmdb.org/t/p/original{{movie.poster_path}}\">\r\n                    <img *ngIf=\"!movie.poster_path\" class=\"movie-image\" alt=\"image poster not available\"\r\n                    src=\"https://cidco-smartcity.niua.org/wp-content/uploads/2017/08/No-image-found.jpg\">\r\n                </div>\r\n            </ion-col>\r\n        </ion-grid>\r\n        <div class=\"info\">\r\n            <div class=\"langs-container\">\r\n                <strong>Languages:</strong>\r\n                <ul>\r\n                    <li *ngFor=\"let langs of movie.spoken_languages;\">\r\n                        <span> {{langs.id}} {{langs.name}}</span>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n            <div class=\"movie-release\">\r\n                <strong>Release: </strong>{{movie.release_date}}\r\n            </div>\r\n            <div class=\"movie-average\">\r\n                <strong>Average</strong> {{movie.vote_average}} / 10\r\n            </div>\r\n            <div class=\"genres-container\" >\r\n                <ul>\r\n                    <li *ngFor=\"let genre of movie?.genres;\" style=\"list-style: none; display: inline;\">\r\n                        <ion-badge  class=\"genre-badge\">{{genre?.name}}</ion-badge>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n            <div class=\"overview-container\">\r\n                <strong>Overview</strong>\r\n                <p class=\"movie-overview\">{{movie.overview}}</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"trailer-container\">\r\n            <strong>Video</strong>\r\n            <iframe *ngIf=\"videoUrl\" width=\"250\" height=\"175\" [src]=\"videoUrl\" frameborder=\"0\" allowfullscreen></iframe>\r\n            <img *ngIf=\"!videoUrl\" class=\"movie-video\" alt=\"video not available\"\r\n            src=\"https://dev.myremotedevelopers.com/assets/img/no-video.gif\">\r\n        </div>\r\n        <div class=\"related-container\">\r\n            <strong class=\"related-list-title\">Related Movies</strong>\r\n            \r\n            <ul class=\"related-list\">\r\n                \r\n                <li *ngFor=\"let similar of related?.results;\">\r\n                    <div class=\"related-card\" (click)=\"onSelect(similar.id)\">\r\n                        <img *ngIf=\"similar?.poster_path\" alt=\"similar movies\"\r\n                        class=\"movie-related-image\"\r\n                        src=\"https://image.tmdb.org/t/p/original{{similar?.poster_path}}\">\r\n                        <img *ngIf=\"!similar?.poster_path\" alt=\"similar movies not available\"\r\n                        class=\"movie-related-image\"\r\n                        src=\"https://cidco-smartcity.niua.org/wp-content/uploads/2017/08/No-image-found.jpg\">\r\n                    </div>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n    \r\n</ion-content> -->\r\n<!--  EXPERIMENTAL -->\r\n<ion-content *ngIf='movie' class=\"movie-details\">\r\n\r\n    <div class=\"movie-data-container\">\r\n        <ion-row>\r\n            <ion-col size-lg=\"4\">\r\n                <div class=\"image-container\">\r\n                    <img *ngIf=\"movie.poster_path\" class=\"movie-image\" alt=\"image poster\"\r\n                    src=\"https://image.tmdb.org/t/p/original{{movie.poster_path}}\">\r\n                    <img *ngIf=\"!movie.poster_path\" class=\"movie-image\" alt=\"image poster not available\"\r\n                    src=\"https://cidco-smartcity.niua.org/wp-content/uploads/2017/08/No-image-found.jpg\">\r\n                </div>\r\n            </ion-col>\r\n            <ion-col size-lg=\"8\">\r\n                <div class=\"info\">\r\n                    <div class=\"langs-container\">\r\n                        <strong>Languages:</strong>\r\n                        <ul>\r\n                            <li *ngFor=\"let langs of movie.spoken_languages;\">\r\n                                <span> {{langs.id}} {{langs.name}}</span>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"movie-release\">\r\n                        <strong>Release: </strong>{{movie.release_date}}\r\n                    </div>\r\n                    <div class=\"movie-average\">\r\n                        <strong>Average</strong> {{movie.vote_average}} / 10\r\n                    </div>\r\n                    <div class=\"genres-container\" >\r\n                        <ul>\r\n                            <li *ngFor=\"let genre of movie?.genres;\" style=\"list-style: none; display: inline;\">\r\n                                <ion-badge  class=\"genre-badge\">{{genre?.name}}</ion-badge>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"overview-container\">\r\n                        <strong>Overview</strong>\r\n                        <p class=\"movie-overview\">{{movie.overview}}</p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"trailer-container\">\r\n                    <iframe *ngIf=\"videoUrl\" width=\"250\" height=\"175\" [src]=\"videoUrl\" frameborder=\"0\" allowfullscreen></iframe>\r\n                    <img *ngIf=\"!videoUrl\" class=\"movie-video\" alt=\"video not available\"\r\n                    src=\"https://dev.myremotedevelopers.com/assets/img/no-video.gif\">\r\n                </div>\r\n            </ion-col>\r\n        </ion-row>\r\n        <div class=\"related-container\">\r\n            <strong class=\"related-list-title\">Related Movies</strong>\r\n            <ul class=\"related-list\">\r\n                <li *ngFor=\"let similar of related?.results;\">\r\n                    <div class=\"related-card\" (click)=\"onSelect(similar.id)\">\r\n                        <img *ngIf=\"similar?.poster_path\" alt=\"similar movies\"\r\n                        class=\"movie-related-image\"\r\n                        src=\"https://image.tmdb.org/t/p/original{{similar?.poster_path}}\">\r\n                        <img *ngIf=\"!similar?.poster_path\" alt=\"similar movies not available\"\r\n                        class=\"movie-related-image\"\r\n                        src=\"https://cidco-smartcity.niua.org/wp-content/uploads/2017/08/No-image-found.jpg\">\r\n                    </div>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n    \r\n</ion-content>\r\n";

/***/ })

}]);
//# sourceMappingURL=src_app_pages_movie-details_movie-details_module_ts.js.map