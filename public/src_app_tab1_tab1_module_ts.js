(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_tab1_tab1_module_ts"],{

/***/ 654:
/*!***************************************************************!*\
  !*** ./src/app/components/movie-list/movie-list.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MovieListComponent": () => (/* binding */ MovieListComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _movie_list_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./movie-list.component.html?ngResource */ 3505);
/* harmony import */ var _movie_list_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movie-list.component.scss?ngResource */ 1579);
/* harmony import */ var _movie_list_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_movie_list_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _api_movies_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../api/movies.service */ 1352);
var _class;






let MovieListComponent = (_class = class MovieListComponent {
  constructor(restApi, router) {
    this.restApi = restApi;
    this.router = router;
    this.noImage = '../../../assets/images/noimage.png';
  }
  recieveSearchName($event) {
    this.searchName = $event.value;
    this.searchMovies();
  }
  searchMovies() {
    this.restApi.searchMovies(this.searchName).subscribe(data => {
      this.movies = data.results.map(movieData => {
        const {
          id
        } = movieData;
        return {
          ...movieData,
          id
        };
      });
    });
  }
  getLatestMovies() {
    this.restApi.getTopRated().subscribe(data => {
      this.topTenMovies = data.results.slice(0, 10).map(movieData => {
        const {
          id
        } = movieData;
        return {
          ...movieData,
          id
        };
      });
    });
  }
  onSelect(id) {
    this.selectedMovie = id;
    this.router.navigate(['movie-details', id]);
  }
  ngOnInit() {
    this.getLatestMovies();
  }
}, _class.ctorParameters = () => [{
  type: _api_movies_service__WEBPACK_IMPORTED_MODULE_2__.MoviesService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router
}], _class);
MovieListComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
  selector: 'app-movie-list',
  template: _movie_list_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [(_movie_list_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default())]
})], MovieListComponent);


/***/ }),

/***/ 9991:
/*!*************************************************************!*\
  !*** ./src/app/components/searchbar/searchbar.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SearchbarComponent": () => (/* binding */ SearchbarComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _searchbar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./searchbar.component.html?ngResource */ 1153);
/* harmony import */ var _searchbar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./searchbar.component.scss?ngResource */ 8596);
/* harmony import */ var _searchbar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_searchbar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 2560);
var _class;




let SearchbarComponent = (_class = class SearchbarComponent {
  constructor() {
    this.searchOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
  }
  ngOnInit() {}
  handleSearch(searchName) {
    if (this.searchName !== null) {
      this.searchName = searchName;
      this.sendName();
    }
  }
  sendName() {
    this.searchOutput.emit(this.searchName);
  }
}, _class.ctorParameters = () => [], _class.propDecorators = {
  searchOutput: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Output
  }]
}, _class);
SearchbarComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
  selector: 'app-searchbar',
  template: _searchbar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [(_searchbar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default())]
})], SearchbarComponent);


/***/ }),

/***/ 2168:
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageModule": () => (/* binding */ Tab1PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 2560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 4666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 6923);
/* harmony import */ var _components_movie_list_movie_list_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/movie-list/movie-list.component */ 654);
/* harmony import */ var _components_searchbar_searchbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/searchbar/searchbar.component */ 9991);









let Tab1PageModule = class Tab1PageModule {};
Tab1PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
  imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild([{
    path: '',
    component: _tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page
  }])],
  exports: [_components_movie_list_movie_list_component__WEBPACK_IMPORTED_MODULE_1__.MovieListComponent],
  declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page, _components_movie_list_movie_list_component__WEBPACK_IMPORTED_MODULE_1__.MovieListComponent, _components_searchbar_searchbar_component__WEBPACK_IMPORTED_MODULE_2__.SearchbarComponent]
})], Tab1PageModule);


/***/ }),

/***/ 6923:
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1Page": () => (/* binding */ Tab1Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page.html?ngResource */ 3852);
/* harmony import */ var _tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab1.page.scss?ngResource */ 65);
/* harmony import */ var _tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 2560);
var _class;




let Tab1Page = (_class = class Tab1Page {
  constructor() {}
}, _class.ctorParameters = () => [], _class);
Tab1Page = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
  selector: 'app-tab1',
  template: _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [(_tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1___default())]
})], Tab1Page);


/***/ }),

/***/ 1579:
/*!****************************************************************************!*\
  !*** ./src/app/components/movie-list/movie-list.component.scss?ngResource ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Imports
var ___CSS_LOADER_API_SOURCEMAP_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/sourceMaps.js */ 9579);
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ 931);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(___CSS_LOADER_API_SOURCEMAP_IMPORT___);
// Module
___CSS_LOADER_EXPORT___.push([module.id, "/** CUSTOM VARIABLES **/\n/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #3880ff;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9;\n}\n\nion-grid {\n  align-content: center;\n  margin: 0 3%;\n}\n\n#myVideo {\n  position: absolute;\n  object-fit: cover;\n  bottom: 60%;\n  min-width: 100%;\n}\n\n.video-container {\n  position: relative;\n  background: rgba(0, 0, 0, 0.5);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  height: 40%;\n}\n.video-container .searchbar {\n  align-self: center;\n}\n\n.top-films {\n  background: #eaeaea;\n  border-radius: 25px;\n}\n.top-films-header {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n  justify-content: center;\n}\n.top-films-header strong {\n  flex-direction: row;\n  font-size: 1.5rem;\n}\n.top-films hr {\n  border-bottom: 2px solid black;\n}\n.top-films .list {\n  display: flex;\n  flex-direction: row;\n}\n.top-films .list ul {\n  padding-inline-start: 10px;\n}\n.top-films .list ul > li {\n  display: flex;\n  flex-direction: row;\n  list-style: none;\n  cursor: pointer;\n}\n.top-films .list ul-image {\n  max-width: 75%;\n}\n\n.movie-container {\n  display: flex;\n  align-content: flex-start;\n  flex-wrap: wrap;\n}\n.movie-container ul {\n  list-style: none;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: center;\n  padding-inline-start: 0;\n}\n\n.movie-row {\n  max-width: 210px;\n}\n.movie-row:hover {\n  cursor: pointer;\n}\n\n.thumbnail-container {\n  width: 11.5rem;\n  height: 18rem;\n  margin: 0px 15px;\n  background-color: #eaeaea;\n  border-radius: 15px;\n}\n.thumbnail-container ion-thumbnail {\n  height: 16rem;\n  width: auto;\n  filter: drop-shadow(0px 5px 2px rgba(0, 0, 0, 0.5));\n  padding: unset;\n}\n.thumbnail-container ion-thumbnail img {\n  min-width: 128px;\n  min-height: 128px;\n  padding: 0.7rem;\n  border-radius: 15px;\n}\n.thumbnail-container ion-thumbnail:hover img {\n  min-width: 128px;\n  min-height: 128px;\n  padding: 0.7rem;\n  border-radius: 15px;\n  filter: drop-shadow(0px 7px 5px rgba(0, 0, 0, 0.5));\n}\n.thumbnail-container .title-container {\n  font-size: 1rem;\n  width: 10rem;\n  margin: 5px 0px 0px 10px;\n  display: flex;\n  justify-content: start;\n  align-content: center;\n}\n.thumbnail-container .title-container span {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.thumbnail-container .title-container span:hover {\n  cursor: pointer;\n  color: #3880ff;\n}", "",{"version":3,"sources":["webpack://./src/theme/variables.scss","webpack://./src/app/components/movie-list/movie-list.component.scss"],"names":[],"mappings":"AAGA,uBAAA;AAGA,0BAAA;AACA;EACE,cAAA;EACA,4BAAA;EACA,qCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,gBAAA;EACA,8BAAA;EACA,uCAAA;EACA,uCAAA;EACA,iDAAA;EACA,oCAAA;EACA,mCAAA;EAEA,eAAA;EACA,6BAAA;EACA,sCAAA;EACA,sCAAA;EACA,gDAAA;EACA,mCAAA;EACA,kCAAA;EAEA,cAAA;EACA,4BAAA;EACA,oCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,cAAA;EACA,4BAAA;EACA,oCAAA;EACA,qCAAA;EACA,+CAAA;EACA,kCAAA;EACA,iCAAA;EAEA,aAAA;EACA,2BAAA;EACA,mCAAA;EACA,oCAAA;EACA,8CAAA;EACA,iCAAA;EACA,gCAAA;EAEA,WAAA;EACA,yBAAA;EACA,gCAAA;EACA,kCAAA;EACA,4CAAA;EACA,+BAAA;EACA,8BAAA;EAEA,aAAA;EACA,2BAAA;EACA,qCAAA;EACA,oCAAA;EACA,8CAAA;EACA,iCAAA;EACA,gCAAA;EAEA,YAAA;EACA,0BAAA;EACA,oCAAA;EACA,mCAAA;EACA,uCAAA;EACA,gCAAA;EACA,+BAAA;ACZF;;AAhEA;EACE,qBAAA;EACA,YAAA;AAmEF;;AAhEA;EACE,kBAAA;EACA,iBAAA;EACA,WAAA;EACA,eAAA;AAmEF;;AAhEA;EACE,kBAAA;EACA,8BAAA;EACA,aAAA;EACA,uBAAA;EACA,mBAAA;EACA,WAAA;AAmEF;AAjEE;EACE,kBAAA;AAmEJ;;AA/DA;EACE,mBAAA;EAGA,mBAAA;AAgEF;AA9DE;EACE,aAAA;EACA,mBAAA;EACA,WAAA;EACA,uBAAA;AAgEJ;AA9DI;EACE,mBAAA;EACA,iBAAA;AAgEN;AA5DE;EACE,8BAAA;AA8DJ;AA5DE;EACE,aAAA;EACA,mBAAA;AA8DJ;AA5DI;EACE,0BAAA;AA8DN;AA7DM;EACE,aAAA;EACA,mBAAA;EACA,gBAAA;EACA,eAAA;AA+DR;AA7DM;EACE,cAAA;AA+DR;;AAzDA;EACE,aAAA;EACA,yBAAA;EACA,eAAA;AA4DF;AA1DE;EACE,gBAAA;EACA,aAAA;EACA,mBAAA;EACA,iBAAA;EACA,uBAAA;EACA,uBAAA;AA4DJ;;AAxDA;EACE,gBAAA;AA2DF;AAzDE;EACE,eAAA;AA2DJ;;AAvDA;EACE,cAAA;EACA,aAAA;EACA,gBAAA;EACA,yBAAA;EACA,mBAAA;AA0DF;AAxDE;EACE,aAAA;EACA,WAAA;EACA,mDAAA;EACA,cAAA;AA0DJ;AAxDI;EACE,gBAAA;EACA,iBAAA;EACA,eAAA;EACA,mBAAA;AA0DN;AArDI;EACE,gBAAA;EACA,iBAAA;EACA,eAAA;EACA,mBAAA;EACA,mDAAA;AAuDN;AAnDE;EACE,eAAA;EACA,YAAA;EACA,wBAAA;EAEA,aAAA;EAEA,sBAAA;EACA,qBAAA;AAqDJ;AAnDI;EACE,mBAAA;EACA,gBAAA;EACA,uBAAA;AAqDN;AAlDI;EACE,eAAA;EACA,cAAA;AAoDN","sourcesContent":["// Ionic Variables and Theming. For more info, please see:\n// http://ionicframework.com/docs/theming/\n\n/** CUSTOM VARIABLES **/\n$margin-sm: 0 0 10px 4%;\n$padding-sm: 5%;\n/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #3880ff;\n  --ion-color-primary-rgb: 56, 128, 255;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255, 255, 255;\n  --ion-color-primary-shade: #3171e0;\n  --ion-color-primary-tint: #4c8dff;\n\n  /** secondary **/\n  --ion-color-secondary: #0cd1e8;\n  --ion-color-secondary-rgb: 12, 209, 232;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255, 255, 255;\n  --ion-color-secondary-shade: #0bb8cc;\n  --ion-color-secondary-tint: #24d6ea;\n\n  /** tertiary **/\n  --ion-color-tertiary: #7044ff;\n  --ion-color-tertiary-rgb: 112, 68, 255;\n  --ion-color-tertiary-contrast: #ffffff;\n  --ion-color-tertiary-contrast-rgb: 255, 255, 255;\n  --ion-color-tertiary-shade: #633ce0;\n  --ion-color-tertiary-tint: #7e57ff;\n\n  /** success **/\n  --ion-color-success: #10dc60;\n  --ion-color-success-rgb: 16, 220, 96;\n  --ion-color-success-contrast: #ffffff;\n  --ion-color-success-contrast-rgb: 255, 255, 255;\n  --ion-color-success-shade: #0ec254;\n  --ion-color-success-tint: #28e070;\n\n  /** warning **/\n  --ion-color-warning: #ffce00;\n  --ion-color-warning-rgb: 255, 206, 0;\n  --ion-color-warning-contrast: #ffffff;\n  --ion-color-warning-contrast-rgb: 255, 255, 255;\n  --ion-color-warning-shade: #e0b500;\n  --ion-color-warning-tint: #ffd31a;\n\n  /** danger **/\n  --ion-color-danger: #f04141;\n  --ion-color-danger-rgb: 245, 61, 61;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255, 255, 255;\n  --ion-color-danger-shade: #d33939;\n  --ion-color-danger-tint: #f25454;\n\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34, 34, 34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255, 255, 255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-tint: #383a3e;\n\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152, 154, 162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255, 255, 255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-tint: #a2a4ab;\n\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244, 244, 244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0, 0, 0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-tint: #f5f6f9;\n}\n","@import \"../../../theme/variables.scss\";\r\n\r\nion-grid {\r\n  align-content: center;\r\n  margin: 0 3%;\r\n}\r\n\r\n#myVideo {\r\n  position: absolute;\r\n  object-fit: cover;\r\n  bottom: 60%;\r\n  min-width: 100%;\r\n}\r\n\r\n.video-container {\r\n  position: relative;\r\n  background: rgba(0, 0, 0, 0.5);\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  height: 40%;\r\n\r\n  .searchbar {\r\n    align-self: center;\r\n  }\r\n}\r\n\r\n.top-films {\r\n  background: #eaeaea;\r\n  // margin-top: 1rem;\r\n  // padding: $padding-sm;\r\n  border-radius: 25px;\r\n\r\n  &-header {\r\n    display: flex;\r\n    flex-direction: row;\r\n    width: 100%;\r\n    justify-content: center;\r\n\r\n    & strong {\r\n      flex-direction: row;\r\n      font-size: 1.5rem;\r\n    }\r\n  }\r\n\r\n  & hr {\r\n    border-bottom: 2px solid black;\r\n  }\r\n  .list {\r\n    display: flex;\r\n    flex-direction: row;\r\n\r\n    & ul {\r\n      padding-inline-start: 10px;\r\n      & > li {\r\n        display: flex;\r\n        flex-direction: row;\r\n        list-style: none;\r\n        cursor: pointer;\r\n      }\r\n      &-image {\r\n        max-width: 75%;\r\n      }\r\n    }\r\n  }\r\n}\r\n\r\n.movie-container {\r\n  display: flex;\r\n  align-content: flex-start;\r\n  flex-wrap: wrap;\r\n\r\n  & ul {\r\n    list-style: none;\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex-wrap: nowrap;\r\n    justify-content: center;\r\n    padding-inline-start: 0;\r\n  }\r\n}\r\n\r\n.movie-row {\r\n  max-width: 210px;\r\n\r\n  &:hover {\r\n    cursor: pointer;\r\n  }\r\n}\r\n\r\n.thumbnail-container {\r\n  width: 11.5rem;\r\n  height: 18rem;\r\n  margin: 0px 15px;\r\n  background-color: #eaeaea;\r\n  border-radius: 15px;\r\n\r\n  ion-thumbnail {\r\n    height: 16rem;\r\n    width: auto;\r\n    filter: drop-shadow(0px 5px 2px rgba(0, 0, 0, 0.5));\r\n    padding: unset;\r\n\r\n    img {\r\n      min-width: 128px;\r\n      min-height: 128px;\r\n      padding: 0.7rem;\r\n      border-radius: 15px;\r\n    }\r\n  }\r\n\r\n  ion-thumbnail:hover {\r\n    img {\r\n      min-width: 128px;\r\n      min-height: 128px;\r\n      padding: 0.7rem;\r\n      border-radius: 15px;\r\n      filter: drop-shadow(0px 7px 5px rgba(0, 0, 0, 0.5));\r\n    }\r\n  }\r\n\r\n  .title-container {\r\n    font-size: 1rem;\r\n    width: 10rem;\r\n    margin: 5px 0px 0px 10px;\r\n    display: -webkit-box;\r\n    display: flex;\r\n    -webkit-box-pack: start;\r\n    justify-content: start;\r\n    align-content: center;\r\n\r\n    & span {\r\n      white-space: nowrap;\r\n      overflow: hidden;\r\n      text-overflow: ellipsis;\r\n    }\r\n\r\n    & span:hover {\r\n      cursor: pointer;\r\n      color: #3880ff;\r\n    }\r\n  }\r\n}\r\n"],"sourceRoot":""}]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___.toString();


/***/ }),

/***/ 8596:
/*!**************************************************************************!*\
  !*** ./src/app/components/searchbar/searchbar.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Imports
var ___CSS_LOADER_API_SOURCEMAP_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/sourceMaps.js */ 9579);
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ 931);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(___CSS_LOADER_API_SOURCEMAP_IMPORT___);
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".search-container {\n  display: flex;\n  justify-content: flex-end;\n  align-items: center;\n  width: 100%;\n  border-radius: 5%;\n}\n\nion-searchbar {\n  --padding: 0.5rem;\n  --margin: 0.5rem;\n  --max-width: 100%;\n  --border-radius: 20px;\n}\n\n.searchbar-input.sc-ion-searchbar-md {\n  --height: 40px;\n  --font-size: 1.25em;\n}\n\n.searchbar-search-icon.sc-ion-searchbar-md {\n  left: 16px;\n  top: 10px;\n  width: 20px;\n  height: 21px;\n}\n\n@media screen and (max-width: 991px) {\n  .search-container {\n    justify-content: space-around;\n  }\n  ion-searchbar {\n    --max-width: 100%;\n  }\n}", "",{"version":3,"sources":["webpack://./src/app/components/searchbar/searchbar.component.scss"],"names":[],"mappings":"AAAA;EACE,aAAA;EACA,yBAAA;EACA,mBAAA;EACA,WAAA;EACA,iBAAA;AACF;;AAEA;EACE,iBAAA;EACA,gBAAA;EACA,iBAAA;EACA,qBAAA;AACF;;AAEA;EACE,cAAA;EACA,mBAAA;AACF;;AAEA;EACE,UAAA;EACA,SAAA;EACA,WAAA;EACA,YAAA;AACF;;AAEA;EACE;IACE,6BAAA;EACF;EACA;IACE,iBAAA;EACF;AACF","sourcesContent":[".search-container {\r\n  display: flex;\r\n  justify-content: flex-end;\r\n  align-items: center;\r\n  width: 100%;\r\n  border-radius: 5%;\r\n}\r\n\r\nion-searchbar {\r\n  --padding: 0.5rem;\r\n  --margin: 0.5rem;\r\n  --max-width: 100%;\r\n  --border-radius: 20px;\r\n}\r\n\r\n.searchbar-input.sc-ion-searchbar-md {\r\n  --height: 40px;\r\n  --font-size: 1.25em;\r\n}\r\n\r\n.searchbar-search-icon.sc-ion-searchbar-md {\r\n  left: 16px;\r\n  top: 10px;\r\n  width: 20px;\r\n  height: 21px;\r\n}\r\n\r\n@media screen and (max-width: 991px) {\r\n  .search-container {\r\n    justify-content: space-around;\r\n  }\r\n  ion-searchbar {\r\n    --max-width: 100%;\r\n  }\r\n}\r\n"],"sourceRoot":""}]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___.toString();


/***/ }),

/***/ 65:
/*!************************************************!*\
  !*** ./src/app/tab1/tab1.page.scss?ngResource ***!
  \************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Imports
var ___CSS_LOADER_API_SOURCEMAP_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/sourceMaps.js */ 9579);
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ 931);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(___CSS_LOADER_API_SOURCEMAP_IMPORT___);
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}", "",{"version":3,"sources":["webpack://./src/app/tab1/tab1.page.scss"],"names":[],"mappings":"AAAA;EACI,gBAAA;EACA,gBAAA;AACJ","sourcesContent":[".welcome-card img {\r\n    max-height: 35vh;\r\n    overflow: hidden;\r\n  }\r\n  "],"sourceRoot":""}]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___.toString();


/***/ }),

/***/ 3505:
/*!****************************************************************************!*\
  !*** ./src/app/components/movie-list/movie-list.component.html?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<video autoplay muted loop id=\"myVideo\">\r\n  <source src=\"assets/video/filmbackground.mp4\" type=\"video/mp4\" />\r\n</video>\r\n\r\n<div class=\"video-container\">\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size-lg=\"3\" size-md=\"4\" size-xs=\"12\" class=\"searchbar\">\r\n        <app-searchbar\r\n          (searchOutput)=\"recieveSearchName($event)\"\r\n        ></app-searchbar>\r\n      </ion-col>\r\n      <ion-col size-lg=\"9\" size-md=\"4\" size-xs=\"12\" class=\"top-films\">\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"top-films-header\">\r\n              <strong>TOP TEN MOVIES</strong>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <hr />\r\n        <ion-row>\r\n          <div class=\"list\">\r\n            <ul\r\n              *ngFor=\"let topMovie of topTenMovies; let i = index\"\r\n              (click)=\"onSelect(topMovie.id)\"\r\n            >\r\n              <li class=\"top-films-item\">\r\n                <!-- {{ i + 1 }}. {{ topMovie.title }} -->\r\n                <div class=\"list-image\">\r\n                  <img\r\n                    *ngIf=\"topMovie.poster_path\"\r\n                    src=\"https://image.tmdb.org/t/p/w500{{\r\n                      topMovie.poster_path\r\n                    }}\"\r\n                  />\r\n                </div>\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </ion-row>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</div>\r\n\r\n<ion-grid>\r\n  <ion-row>\r\n    <ion-col size-lg=\"9\" size-md=\"8\" size-xs=\"12\">\r\n      <div class=\"movie-container\">\r\n        <ul *ngFor=\"let movie of movies\" (click)=\"onSelect(movie.id)\">\r\n          <li class=\"movie-row\">\r\n            <div class=\"thumbnail-container\">\r\n              <ion-thumbnail>\r\n                <img\r\n                  *ngIf=\"movie.poster_path\"\r\n                  src=\"https://image.tmdb.org/t/p/w500{{ movie.poster_path }}\"\r\n                />\r\n                <img *ngIf=\"!movie.poster_path\" [src]=\"noImage\" />\r\n              </ion-thumbnail>\r\n              <div class=\"title-container\">\r\n                <span>{{ movie.title }}</span>\r\n              </div>\r\n            </div>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-grid>\r\n";

/***/ }),

/***/ 1153:
/*!**************************************************************************!*\
  !*** ./src/app/components/searchbar/searchbar.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"search-container\">\r\n  <ion-searchbar\r\n    placeholder=\"Movie Name\"\r\n    type=\"text\"\r\n    (keydown.enter)=\"handleSearch(searchName)\"\r\n    #searchName\r\n  ></ion-searchbar>\r\n</div>\r\n";

/***/ }),

/***/ 3852:
/*!************************************************!*\
  !*** ./src/app/tab1/tab1.page.html?ngResource ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-content>\n  <app-movie-list></app-movie-list>\n</ion-content>\n\n";

/***/ })

}]);
//# sourceMappingURL=src_app_tab1_tab1_module_ts.js.map